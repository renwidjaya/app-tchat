import React from 'react';

import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import {Ionicons} from '@expo/vector-icons'


import HomeScreen from './src/screens/home';
import MessageScreen from './src/screens/message';
import PostsScreen from './src/screens/posts';
import Notificationscreen from './src/screens/notifications';
import ProfileScreen from './src/screens/profile';


import LoadingScreen from './src/screens/loading';
import LoginScreen from './src/screens/login';
import RegisterScreen from './src/screens/register';

import * as firebase from 'firebase';

var firebaseConfig = {
  apiKey: "AIzaSyB0A_9Dc1MuHphp5QvBRjFRA0I0C_kIgSs",
  authDomain: "temanrendy-36950.firebaseapp.com",
  databaseURL: "https://temanrendy-36950.firebaseio.com",
  projectId: "temanrendy-36950",
  storageBucket: "temanrendy-36950.appspot.com",
  messagingSenderId: "544630619251",
  appId: "1:544630619251:web:9c0da16467c903944b3e06",
  measurementId: "G-12DJBZ2S2W"
};

firebase.initializeApp(firebaseConfig);

const AppContainer = createStackNavigator(
  {
    default: createBottomTabNavigator(
      {
        Home: {
          screen: HomeScreen,
          navigationOptions: {
            tabBarIcon: ({ tintColor }) => <Ionicons name='ios-home' size={24} color={tintColor}></Ionicons>
          }
        },
        Message: {
          screen: MessageScreen,
          navigationOptions: {
            tabBarIcon: ({ tintColor }) => <Ionicons name='ios-chatboxes' size={24} color={tintColor}></Ionicons>
          }
        },
        Posts: {
          screen: PostsScreen,
          navigationOptions: {
            tabBarIcon: ({ tintColor }) =>
            <Ionicons 
              name='ios-add-circle' 
              size={50} 
              color='#E9446A'
              style={{ 
                  shadowColor: '#E9446A', 
                  shadowOffseet: { width: 0, height: 0 },
                  shadowRadius: 10,
                  shadowOpacity: 0.0
                }}
            />
          }
        },
        Notifications: {
          screen: Notificationscreen,
          navigationOptions: {
            tabBarIcon: ({ tintColor }) => <Ionicons name='ios-notifications' size={24} color={tintColor}></Ionicons>
          }
        },
        Profile: {
          screen: ProfileScreen,
          navigationOptions: {
            tabBarIcon: ({ tintColor }) => <Ionicons name='ios-person' size={24} color={tintColor}></Ionicons>
          }
        }
      },
      {
        defaultNavigationOptions:{
          tabBarOnPress: ({navigation, defaultHandler}) => {
            if (navigation.state.key === 'Post') {
              navigation.navigate('postModal')
            } else {
              defaultHandler()
            }
          }
        },
        tabBarOptions: {
          activeTintColor: '#161F3D',
          inactiveTintColor: '#B8B8C4',
          showLabel: false
        }
      }
    ),
    postModal:{
      screen: PostsScreen
    }
  },
  {
    mode: 'modal',
    headerMode: 'none',
  }
);

const AuthStack = createStackNavigator({
  Login: LoginScreen,
  Register: RegisterScreen
});

export default createAppContainer(
  createSwitchNavigator(
    {
      Loading: LoadingScreen,
      App: AppContainer,
      Auth: AuthStack
    },
    {
      initialRouteName: "Loading"
    }
  )
);
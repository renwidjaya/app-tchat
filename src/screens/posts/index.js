import React, { Component } from 'react';
import { Text, StyleSheet, View, SafeAreaView, TouchableOpacity, Image, TextInput } from 'react-native';
import {Ionicons} from '@expo/vector-icons';

export default class Posts extends Component {
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity>
                        <Ionicons name='md-arrow-back' size={24} color='#D8D9DB'></Ionicons>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={{ fontWeight: '500' }}>Posts</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.InputContainer}>
                    <Image source={require('../../assets/images/ic4.png')} style={styles.avatar}></Image>
                    <TextInput 
                    autoFocus={true}
                    multiline={true}
                    numberOfLines={4}
                    style={{flex: 1,}}
                    placeholder="Want to share something..."

                    />
                </View>
                <TouchableOpacity style={styles.photo}>
                    <Ionicons name="md-camera" size={32} color='#D8D9D8' />
                </TouchableOpacity>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        top: 24
    },
    header:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 32,
        paddingVertical: 12,
        borderBottomWidth: 1,
        borderBottomColor: '#D8D9DB'
    },
    InputContainer:{
        margin: 32,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    avatar:{
        width: 48,
        height: 48,
        borderRadius: 24,
        marginRight: 16
    },
    photo:{
        alignItems: 'flex-end',
        marginHorizontal: 32
    }
})

import React, { Component } from 'react'
import { Text, StyleSheet, View, TextInput, TouchableOpacity, ScrollView, StatusBar, LayoutAnimation } from 'react-native'
import * as firebase from 'firebase';
import { Ionicons } from '@expo/vector-icons'

export default class Register extends Component {

    static navigationOptions = {
        header: null
    }

    state = {
        name: "",
        email: "",
        password: "",
        errorMessage: null
    }

    handleSignup = () => {
        const {email, password} = this.state

        firebase
        .auth()
        .createUserWithEmailAndPassword(this.state.email, this.state.password)
        .then(userCredentials => {
            return userCredentials.user.updateProfile({
                displayName: this.state.name
            });
        })
        .catch(error => this.setState({ errorMessage: error.message }));
    }

    render() {
        LayoutAnimation.easeInEaseOut();
        return (
            <ScrollView style={{flex: 1}}>
            <View style={styles.container}>
                <StatusBar barStyle='light-content'></StatusBar>

                
                <TouchableOpacity style={styles.back} onPress={() => this.props.navigation.goBack()}>
                <Ionicons name="ios-arrow-round-back" size={32} color='#FFF' />
                </TouchableOpacity>
                
                <View style={{ position: 'absolute', top:50, alignItems: 'center', width: '100%' }}>
                    <Text style={styles.greeting}> 
                        {'Hello! \nSign up to get started.'}
                    </Text>
                    <TouchableOpacity style={styles.avatar}>
                        <Ionicons
                            name='ios-add'
                            size={40}
                            color='#FFF'
                            style={{ marginTop: 6, marginLeft: 2 }}
                        ></Ionicons>
                    </TouchableOpacity>
                </View>

                <View style={styles.errorMessage}>
                    { this.state.errorMessage && <Text style={styles.error}>{this.state.errorMessage}</Text>}
                </View>

                <View style={styles.form}>
                    <View>
                        <Text style={styles.inputTitle}>Full Name</Text>
                        <TextInput style={styles.input} 
                        autoCapitalize="none"
                        onChangeText={name => this.setState({name})}
                        value={this.state.name}
                        ></TextInput>
                    </View>
                    <View style={{marginTop: 32}}>
                        <Text style={styles.inputTitle}>Email Address</Text>
                        <TextInput style={styles.input} 
                        autoCapitalize="none"
                        onChangeText={email => this.setState({email})}
                        value={this.state.email}
                        ></TextInput>
                    </View>
                    <View style={{marginTop: 32}}>
                        <Text style={styles.inputTitle}>Password</Text>
                        <TextInput style={styles.input} 
                        secureTextEntry
                        autoCapitalize="none"
                        onChangeText={password => this.setState({password})}
                        value={this.state.password}
                        ></TextInput>
                    </View>
                </View>

                <TouchableOpacity style={styles.button} onPress={this.handleSignup}> 
                    <Text style={{ color: '#fff', fontWeight: "500" }}>Sign In</Text>
                </TouchableOpacity>

                <TouchableOpacity 
                    style={{ alignSelf: 'center', marginTop: 32 }}
                    onPress={() => this.props.navigation.navigate("Login")}
                    >
                    <Text style={{ color: '#414959', fontSize: 13 }}>
                        New to SocialApp <Text style={{ color: '#E9446A', fontWeight: "500" }}>Sign</Text>
                    </Text>
                </TouchableOpacity>
            </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
    },
    greeting:{
        marginTop: 32,
        fontSize: 18,
        fontWeight: "400",
        textAlign: 'center'
    },
    errorMessage:{
        height: 72,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 30
    },
    error:{
        color: '#E9446A',
        fontSize: 13,
        textAlign: 'center'
    },
    form:{
        marginTop: 250,
        marginBottom: 48,
        marginHorizontal: 30
    },
    inputTitle:{
        color: '#8A8F9E',
        fontSize: 10,
        textTransform: 'uppercase'
    },
    input:{
        borderBottomColor: '#8A8F9E',
        borderBottomWidth: StyleSheet.hairlineWidth,
        height: 40,
        fontSize: 15,
        color: '#161F3D'
    },
    button:{
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#E9446A',
        height: 52,
        marginHorizontal: 30
    },
    back:{
       position: 'absolute',
       top: 60,
       left: 32,
       width: 32,
       height: 32,
       borderRadius: 16,
       backgroundColor: 'rgba(21, 22, 48, 0.1)',
       alignItems: 'center',
       justifyContent: 'center' 
    },
    avatar:{
        width: 100,
        height: 100,
        borderRadius: 50,
        color: '#E1E2E6',
        marginTop: 48,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
